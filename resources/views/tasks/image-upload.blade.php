<!DOCTYPE html>
<html lang="en">
<head>
    <title>Watermark</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{ URL::asset("app-assets/login/bootstrap/css/bootstrap.min.css")}}">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{ URL::asset("app-assets/css/dropify.css")}}">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{ URL::asset("app-assets/login/css/util.css")}}">
    <link rel="stylesheet" type="text/css" href="{{ URL::asset("app-assets/login/css/main.css")}}">
    <!--===============================================================================================-->
    <style>
        #main_word {
            text-align: center;
            position: relative;
            bottom: 10px;
            color: #403767;
        }
    </style>
</head>
<body>
<div class="limiter">
    <div class="container-login100">
        <div class="wrap-login100 p-t-50 p-b-90">
            {{--<form>--}}

                <h5 id="main_word">Please upload</h5>
                <br>

                <div class="col-sm-12" style="margin-bottom: 20px;">
                    <input type="file" id="main_image" name='file' data-allowed-file-extensions="jpg" data-max-file-size="2M"/>
                </div>

                <div class="container-login100-form-btn m-t-17">
                    <button class="login100-form-btn" id="submit">
                        Submit
                    </button>
                </div>

            {{--</form>--}}
        </div>
    </div>
</div>

<script src="{{ URL::asset("app-assets/login/jquery/jquery-3.2.1.min.js")}}"></script>
<!--===============================================================================================-->
<script src="{{ URL::asset("app-assets/login/daterangepicker/moment.min.js")}}"></script>
<!--===============================================================================================-->
<script src="{{ URL::asset("app-assets/js/scripts/dropify.js")}}"></script>
<script src="{{ URL::asset("app-assets/js/scripts/FileSaver.min.js")}}"></script>

<script>

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': "{{ csrf_token() }}"
        }
    });

    // Init File Service
    $('#main_image').dropify();

    // Submit data
    $(document).on('click', 'button#submit', function () {
        let form_data = new FormData();

        form_data.append('img', $('#main_image').prop('files')[0]);

        $.ajax({
            type: "POST",
            url: '{{ route('task.image.upload') }}',
            processData: false,
            contentType: false,
            data: form_data,
            success: function (response) {
                window.open(response, '_blank');
            },
            error: function (data) {
                console.log(data['responseJSON']['message']);
            }
        });
    });

</script>
</body>
</html>
