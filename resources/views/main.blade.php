<!DOCTYPE html>
<html lang="en">
<head>
    <title>Watermark</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{ URL::asset("app-assets/login/bootstrap/css/bootstrap.min.css")}}">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{ URL::asset("app-assets/fonts/line-awesome/css/line-awesome-font-awesome.css")}}">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{ URL::asset("app-assets/css/dropify.css")}}">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{ URL::asset("app-assets/login/css/util.css")}}">
    <link rel="stylesheet" type="text/css" href="{{ URL::asset("app-assets/login/css/main.css")}}">
    <!--===============================================================================================-->
    <style>
    .center {
        align-items: center;
        display: flex;
        justify-content: center;
        height: 100vh;
    }
    .center > a {
        margin: 5%;
        font-size: 25px;
        color: #0073ff;
        font-weight: 100;
        text-decoration: underline;
    }
    .center > a:hover {
        text-decoration: none;
    }
    </style>
</head>
<body>

<div class="center">
    <a href="{{ route('task.image') }}">Watermark</a>
    <a href="{{ route('task.db') }}">DB</a>
</div>

<script src="{{ URL::asset("app-assets/login/jquery/jquery-3.2.1.min.js")}}"></script>
<!--===============================================================================================-->
<script src="{{ URL::asset("app-assets/login/daterangepicker/moment.min.js")}}"></script>
<!--===============================================================================================-->
<script src="{{ URL::asset("app-assets/js/scripts/dropify.js")}}"></script>
<script src="{{ URL::asset("app-assets/js/scripts/FileSaver.min.js")}}"></script>

</body>
</html>
