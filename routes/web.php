<?php


Route::get('/', 'MainController@getIndex')->name('main');

/**
 * For testing of tasks
 */
Route::group(['namespace' => 'Tasks', 'prefix' => 'task'],function () {

    /**
     * For DB test
     */
    Route::group(['namespace' => 'DBTask'],function () {
        Route::get('db',                 'DBTaskController@getIndex')->name('task.db');
    });

    /**
     * For color definition
     */
    Route::group(['namespace' => 'ImageTask'],function () {
        Route::get('image',              'ImageTaskController@getIndex')->name('task.image');

        Route::post('upload-image',      'ImageTaskController@getImage')->name('task.image.upload');
    });

});