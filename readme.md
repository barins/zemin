## Zemin Project

##### Run:
-  command: composer update
-  command: php artisan migrate
-  command: php artisan storage:link

-----------------------------------

##### Task #1
Assignments, identify the main color of the image 
and insert a suitable watermark on it.

- url: http://127.0.0.1:8000/task/image
- url: [Images for testing, please download](https://goo-gl.su/DREn5l)


##### Task #2

Get data from an excel file that contains at least 
10,000 records and paste it into the database.
The process should complete in 10 seconds

- url: http://127.0.0.1:8000/task/db