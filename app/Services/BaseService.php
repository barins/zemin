<?php

namespace App\Services;


abstract class BaseService
{
    /**
     * BaseService constructor
     */
    public function __construct()
    {
        //
    }

    /**
     * @return mixed
     */
    abstract function build();
}