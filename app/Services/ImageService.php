<?php

namespace App\Services;

use Intervention\Image\Facades\Image;

/**
 * Class ImageService
 *
 * @property Image $result
 * @property integer $main_color
 * @property Image $watermark
 * @property string $watermark_color
 * @property integer $resizePercentage
 * @property object $image
 *
 * @package App\Services
 */
class ImageService extends BaseService
{
    /**
     * @var
     */
    private $result;

    /**
     * @var
     */
    private $main_color;

    /**
     * @var
     */
    private $watermark;

    /**
     * @var
     */
    private $watermark_color;

    /**
     * 30% less then an actual image (play with this value)
     */
    private $resizePercentage = 30;

    /**
     * @var
     */
    private $image;

    /**
     * @param $image
     *
     * @return $this
     */
    public function setImage($image)
    {
     $this->image = $image;

     return $this;
    }

    /**
     * @return $this
     */
    public function build()
    {
        $this->colorDefinition();

        $this->colorWatermark();

        $this->setSizeOfWatermark();

        $this->setWatermark();

        $this->save();

        return $this;
    }

    /**
     * For color definition
     */
    private function colorDefinition()
    {
        $image = imagecreatefromjpeg($this->image);
        $thumb = imagecreatetruecolor(1,1);

        imagecopyresampled(
            $thumb,
            $image,
            0,
            0,
            0,
            0,
            1,
            1,
            imagesx($image),
            imagesy($image)
        );

        $this->main_color = strtoupper(dechex(imagecolorat($thumb,0,0)));
    }

    private function getWatermark()
    {
      return
          Image::make(
              public_path("app-assets/images/watermark/watermark-{$this->watermark_color}.png")
          );
    }

    /**
     * For color of watermark
     */
    private function colorWatermark()
    {
        $this->watermark_color =
              hexdec(substr($this->main_color,0,2))
            + hexdec(substr($this->main_color,2,2))
            + hexdec(substr($this->main_color,4,2))
            > 382
            ? 'dark' : 'bright';
    }

    /**
     * To determine the appropriate size of the watermark
     */
    private function setSizeOfWatermark()
    {
        $this->image = Image::make($this->image);

        $this->watermark = $this->getWatermark();

        $watermarkSize = round(
            $this->image->width() * ( (100 - $this->resizePercentage) / 100 ), 2);

        $this->watermark->resize($watermarkSize, null, function ($constraint) {
            $constraint->aspectRatio();
        });
    }

    /**
     * For insert a watermark
     */
    private function setWatermark()
    {
        $this->result = $this->image->insert($this->watermark, 'center');
    }

    /**
     * For save result
     */
    private function save()
    {
        $this->result->save(storage_path("app/public/result.png"));
    }

    /**
     * For get result
     *
     * @return static
     */
    public function get()
    {
        return response(
            \Storage::url('result.png')
        );
    }



}