<?php

namespace App\Services;

use App\Models\Test;
use fastreading\excel\Excel;

ini_set('memory_limit', '256M');

set_time_limit(10);

/**
 * Class DBUploadService
 *
 * @package App\Services
 */
class DBUploadService extends BaseService
{
    /**
     * @var
     */
    private $time;

    /**
     * @var
     */
    private $timeOfExport;

    /**
     * @var
     */
    private $path;

    /**
     * @var array
     */
    private $data = [];

    /**
     * Run Stopwatch
     */
    public function runStopwatch()
    {
        $this->time = microtime(true);
    }

    /**
     * @return mixed
     *
     * @throws \Exception
     */
    public function build()
    {
        $this->runStopwatch();

        $this->setPath();

        $this->prepareTable();

        $this->export();

        $this->save();

        return
            $this->getResponse();

    }

    private function setPath()
    {
        $this->path = public_path('temp/Sample.xls');
    }

    private function prepareTable()
    {
        Test::truncate();
    }

    private function export()
    {
        try {
            $data = Excel::load($this->path)->getFile();

            $result = [];

            if($data) {
                foreach ($data as $key => $record) {
                        $result[] = [
                            'a' => $record['0'],
                            'b' => $record['1'],
                            'c' => $record['2'],
                            'd' => $record['3'],
                            'e' => $record['4'],
                            'f' => $record['5']
                        ];
                }

                $this->data = $result;

                $this->timeOfExport = round(microtime(true) - $this->time, 1);
            }
        } catch (\Exception $e) {
            throw new \Exception('Bad luck)');
        }
    }

    private function save()
    {
        $this->data = array_chunk($this->data, 10500);

        foreach ($this->data as $data) {
            Test::insert($data);
        }
    }

    /**
     * @return mixed
     */
    private function getResponse()
    {
        $response = $this->getTime();

        $response .= "</br> Count records: " . Test::count();

        return $response;
    }


    /**
     * @return string
     */
    private function getTime()
    {
        $now = round(microtime(true) - $this->time, 1);
        $sql = $now - $this->timeOfExport;

        return
            "Execution time: </br>" .
            "Parse data: {$this->timeOfExport} seconds </br>" .
            "SQL query: $sql seconds </br>" .
            "All: $now seconds </br>";

    }
}