<?php

namespace App\Http\Controllers\Tasks\DBTask;

use App\Http\Controllers\Controller;

use App\Services\DBUploadService;

/**
 * Class DBTaskController
 *
 * @package App\Http\Controllers\Tasks\DBTask
 */
class DBTaskController extends Controller
{
    /**
     * @url "http://127.0.0.1:8000/task/db"
     *
     * @return  DBUploadService
     *
     * @throws \Exception
     */
    public function getIndex()
    {
        $service = New DBUploadService();

        return
            $service
                ->build();
    }
}
