<?php

namespace App\Http\Controllers\Tasks\ImageTask;

use App\Http\Requests\UploadImageRequest;

use App\Http\Controllers\Controller;
use App\Services\ImageService;

/**
 * Class ImageTaskController
 * @package App\Http\Controllers\Tasks\ImageTask
 */
class ImageTaskController extends Controller
{
    /**
     * @url "http://127.0.0.1:8000/task/image"
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getIndex()
    {
        return view('tasks.image-upload');
    }

    /**
     * @param UploadImageRequest $request
     *
     * @rules required|image|mimes:jpeg|max:2000
     *
     * @url "http://127.0.0.1:8000/task/upload-image"
     *
     * @return ImageService
     */
    public function getImage(UploadImageRequest $request)
    {
        $service = New ImageService;

        return
            $service
                ->setImage($request->file('img'))
                ->build()
                ->get();
    }
}
