<?php

namespace App\Http\Controllers;

class MainController extends Controller
{
    /**
     * @url "http://127.0.0.1:8000"
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getIndex()
    {
        return view('main');
    }
}
