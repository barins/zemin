<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Test extends Model
{
    /**
     * Class Test
     *
     * @package App\Models
     *
     * @property integer id
     * @property string a
     * @property string b
     * @property string c
     * @property string d
     * @property string e
     * @property string f
     */


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */


    /**
     * @var string
     *
     * Name Table
     */
    protected $table = 'test';


    protected $fillable = [
        'a', 'b', 'c', 'd', 'e', 'f'
    ];
}
